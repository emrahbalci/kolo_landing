{
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

    require('owl.carousel');

    const common = require('./utils/common.js');

    //mobile menu
    common.mobileMenu();

    //fix header
    common.fixHeader();

    common.scrollAnimate();

    $('.homepage-slider').owlCarousel({
        loop: false,
        nav: false,
        dots: true,
        items: 1,
        smartSpeed: 1000,
        animateOut: 'fadeOut'
    });
}
