let modules = {}

modules.mobileMenu = () => {
    $(document).on('click', '.toggle-menu', function() {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
    })
};

modules.scrollAnimate = () => {
    var $mainMenu = $('.main-menu');
    $mainMenu.find('a').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href');
        $('.toggle-menu').removeClass('active');
        $mainMenu.removeClass('active');
        $('html, body').animate({ scrollTop: $(id).offset().top - 100 }, 500);
    })
}

modules.fixHeader = () => {
    var $header = $('#header');
    $(window).on('load scroll', function () {
        var $scrollTop = $(window).scrollTop();

        if ($scrollTop > 10) {
            $header.addClass('fixed');
        } else {
            $header.removeClass('fixed');
        }

    });
};


module.exports = modules
